# p1mqtt

p1mqtt is a bridge between p1 smart energy meter and MQTT

# example use

Run these commands (or start with your init system).

	$ p1mqtt -d /dev/ttyUSB1 -v &

This will export the airconditioner properties into MQTT,
under `p1/`

# doc

https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_a727fce1f1.pdf

https://jensd.be/1183/linux/read-data-from-the-belgian-digital-meter-through-the-p1-port
